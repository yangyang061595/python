from multiprocessing import Process,Array,Value

import time
start_time = time.time()


def calcul_long(processId, res,nombre):
    n = nombre.value - processId 
    while n>0:
        res[processId] +=n
        n -= 4 

if __name__ == "__main__":

    nombre = Value('i',1000000) 
    res = Array('i', 4)

    p1 = Process(target=calcul_long, args=(0,res,nombre))
    p2 = Process(target=calcul_long, args=(1,res,nombre))
    p3 = Process(target=calcul_long, args=(2,res,nombre))
    p4 = Process(target=calcul_long, args=(3,res,nombre))

    p1.start()
    p2.start()
    p3.start()
    p4.start()

    p1.join()
    p2.join()
    p3.join()
    p4.join()

    print(res[:])
    print(res[0]+res[1]+res[2]+res[3])
    print("--- %s seconds ---" % (time.time() - start_time))
