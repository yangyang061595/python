from PIL import Image
import numpy as np
from scipy import ndimage
import random as rn
import asyncio
from enum import Enum
import sys 

class Direction(Enum):
    LEFT  = 1
    RIGHT = 2
    UP = 3
    DOWN = 4 

WIDTH, HEIGHT = 360 ,480

data = np.full((WIDTH,HEIGHT,3),255,dtype = np.uint8)
mc =[ [1,2,1],[2,4,2],[1,2,1]
        ]
data_lock = asyncio.Lock() 
class Fourmis:
    global WIDTH,HEIGHT 
    global mc,data_lock
    def __init__(self, Cr,Cv,Cb,Sr,Sv,Sb,Pg,Pd,Pt,isDo,Ps,NbIter):
        self.Cr = Cr
        self.Cv = Cv
        self.Cb = Cb
        self.Sr = Sr 
        self.Sv = Sv 
        self.Sb = Sb 
        self.Pg = Pg 
        self.Pd = Pd 
        self.Pt = Pt 
        self.isDo = isDo 
        self.Ps = Ps 
        self.NbIter = NbIter 

        self.dir = Direction.RIGHT 
        self.x = rn.randint(0,WIDTH -1 )
        self.y = rn.randint(0,HEIGHT -1 )

    async def move(self):
        for _ in range(NbIter):
            x_moin1 = WIDTH-1 if self.x-1 <0 else self.x-1;
            x_plus1 = 0 if self.x+1 >= WIDTH else self.x+1;

            y_moin1 = HEIGHT-1  if self.y -1 <0 else self.y-1;
            y_plus1 = 0 if self.y+1 > HEIGHT-1 else self.y+1;
        
            voisinList = [ [x_moin1,y_moin1],[self.x,y_moin1], [x_plus1,y_moin1],
                        [x_moin1,self.y] ,  [x_plus1,self.y],
                        [x_moin1,y_plus1],[self.x ,y_plus1],[x_plus1,y_plus1]]
            attire = False
            for voisin in voisinList:
                indexX = voisin[0]
                indexY = voisin[1]
                Sr = data[indexX,indexY,0]
                Sv = data[indexX,indexY,1]
                Sb = data[indexX,indexY,2]
                if  calculDiffLum(Sr,Sv,Sb,Cr,Cv,Cb) > 40:
                    if rn.random() > Ps:
                        self.x = indexX
                        self.y = indexY
                        attire = True
                        break
            if not attire :
                prob = rn.random()
                if isDo:  #deplacement oblique 8 voisions
                    if self.dir == Direction.RIGHT : # verifier  les  3 voisins a droite  
                        if prob < Pg: #deplacer a droite haute 
                            self.x = x_plus1    
                            self.y = y_moin1 
                            self.dir = Direction.UP 
                        elif prob < (Pg+Pd) : # deplacer droit
                            self.x = x_plus1 
                        else:#deplacer a droite bas 
                            self.x = x_plus1
                            self.y = y_plus1 
                            self.dir = Direction.DOWN 
                    elif self.dir == Direction.LEFT : #verifier les 3 voisin a gauche 
                        if prob < Pg: 
                            self.x = x_moin1    
                            self.y = y_plus1 
                            self.dir = Direction.DOWN  
                        elif prob < (Pg+Pd) : 
                            self.x = x_moin1 
                        else:
                            self.x = x_moin1 
                            self.y = y_moin1
                            self.dir = Direction.UP 
                    elif self.dir == Direction.UP : #verifier les 3 voisin a gauche 
                        if prob < Pg: 
                            self.x = x_moin1    
                            self.y = y_moin1 
                            self.dir = Direction.LEFT
                        elif prob < (Pg+Pd) : 
                            self.y = y_moin1 
                        else:
                            self.x = x_plus1 
                            self.y = y_moin1
                            self.dir = Direction.RIGHT
                    elif self.dir == Direction.DOWN: #verifier les 3 voisin a gauche 
                        if prob < Pg: 
                            self.x = x_plus1    
                            self.y = y_plus1 
                            self.dir = Direction.RIGHT
                        elif prob < (Pg+Pd) : 
                            self.y = y_plus1 
                        else:
                            self.x = x_moin1 
                            self.y = y_plus1
                            self.dir = Direction.LEFT


                else: #deplacement droit
                    if self.dir == Direction.RIGHT:
                        if prob< Pg:
                            self.y = y_moin1
                            self.dir = Direction.UP
                        elif prob <(Pg+Pd):
                            self.x = x_plus1
                        else:
                            self.y = y_plus1
                            self.dir = Direction.DOWN
                    if self.dir == Direction.LEFT:
                        if prob< Pg:
                            self.y = y_plus1
                            self.dir = Direction.DOWN
                        elif prob <(Pg+Pd):
                            self.y = y_moin1
                        else:
                            self.x = x_moin1
                            self.dir = Direction.UP
                    if self.dir == Direction.UP:
                        if prob< Pg:
                            self.x = x_moin1
                            self.dir = Direction.LEFT
                        elif prob <(Pg+Pd):
                            self.y = y_moin1
                        else:
                            self.x = x_plus1
                            self.dir = Direction.RIGHT
                    if self.dir == Direction.DOWN:
                        if prob< Pg:
                            self.x = x_plus1
                            self.dir = Direction.RIGHT
                        elif prob <(Pg+Pd):
                            self.y = y_plus1
                        else:
                            self.x = x_moin1
                            self.dir = Direction.LEFT

            async with data_lock:
                data[self.x,self.y,0] = self.Cr
                data[self.x,self.y,1] = self.Cv
                data[self.x,self.y,2] = self.Cb
                filterNeighbors(self.x,self.y)
        
def calculLum(R,G,B):
    return 0.242*R + 0.7152*G + 0.0722*B;

def calculDiffLum(Sr,Sv,Sb,R,V,B):
    return abs(calculLum(Sr,Sv,Sb) - calculLum(R,V,B));
        
        
def filterNeighbors(x,y):
    global data
    global WIDTH
    global HEIGHT
    global mc
    min_x = 0 if x-2 <=0 else x -2
    min_y = 0 if y-2 <=0 else y-2
    max_x = WIDTH-2 if x +2 >= WIDTH else x+2
    max_y = HEIGHT-2  if y+2 >= HEIGHT else y+2
    data[min_x:max_x,y:max_y,0]  =  ndimage.convolve(data[min_x:max_x,y:max_y,0], mc, mode='constant', cval=0.0)
    data[min_x:max_x,y:max_y,1]  =  ndimage.convolve(data[min_x:max_x,y:max_y,1], mc, mode='constant', cval=0.0)
    data[min_x:max_x,y:max_y,2]  =  ndimage.convolve(data[min_x:max_x,y:max_y,2], mc, mode='constant', cval=0.0)





if __name__ == '__main__':
    print( 'Number of arguments:', len(sys.argv), 'arguments.')
    params =[]
    with open(sys.argv[1],"rt") as fic:
        lines = fic.readlines()
        for line in lines:
            Cr = int(line.split(";")[0])
            Cv = int(line.split(";")[1])
            Cb = int( line.split(";")[2])
            Sr = int(line.split(";")[3])
            Sv = int( line.split(";")[4])
            Sb = int(line.split(";")[5])
            Pg = float(line.split(";")[6])
            Pd = float(line.split(";")[7])
            Pt = float(line.split(";")[8])
            isDo = bool( line.split(";")[9])
            Ps  = float(line.split(";")[10])
            NbIter = int(line.split(";")[11])
            params.append( [ Cr,Cv,Cb,Sr,Sv,Sb,Pg,Pd,Pt,isDo,Ps,NbIter])

    fourmisList = [Fourmis(i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7],i[8],i[9],i[10],i[11]) for i in params]


    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        asyncio.wait([ f.move() for f in fourmisList
            ] ) )

    img = Image.fromarray(data,'RGB')
    img.save('my.png')
    img.show()
