import random
import sys
from threading import Thread, RLock
import time

verrou = RLock()
res = [0,0,0,0]
nombre = 1000000
start_time = time.time()

class Afficheur(Thread):

    """Thread chargé simplement de faire un somme avec un nombre en 4 threads."""

    def __init__(self,threadId):
        Thread.__init__(self)
        self.threadId = threadId
        self.n = nombre - self.threadId
    def run(self):
        """Code à exécuter pendant l'exécution du thread."""
        while self.n > 0:
            with verrou:
                #sys.stdout.write(str(self.n)+"\n")
                #sys.stdout.flush()
                res[self.threadId] += self.n
                self.n -= 4 

# Création des threads
thread_1 = Afficheur(0)
thread_2 = Afficheur(1)
thread_3 = Afficheur(2)
thread_4 = Afficheur(3)

# Lancement des threads
thread_1.start()
thread_2.start()
thread_3.start()
thread_4.start()

# Attend que les threads se terminent
thread_1.join()
thread_2.join()
thread_3.join()
thread_4.join()

print(res)
print(res[0]+res[1]+res[2]+res[3])
print("--- %s seconds ---" % (time.time() - start_time))
