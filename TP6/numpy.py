import numpy as np


def tableauDim3():
    tableau = np.random.randint(0, 100, size=(4, 3, 2))
    print("Taille du tableau :", tableau.size)
    print("Shape du tableau :", tableau.shape)
    print("Dimension du tableau :", tableau.ndim)
    print("Type des données dans le tableau :", tableau.dtype)
    print("Longueur de chaque élément en bytes :", tableau.itemsize)
    print("Les données en mémoire :", tableau.data)
    print("Le tableau en entier :", tableau)


def multiplyOrDot():

    matriceA = np.random.randint(0, 8, size=(3, 3))
    matriceB = np.random.randint(2, 10, size=(3, 3))
    print("Matrice A:", matriceA)
    print("Matrice B:", matriceB)
    print("Multiplication :", matriceA*matriceB)
    print("Dot :", np.dot(matriceA, matriceB))
    print("Transposé :", np.transpose(matriceA))


def inverseAndDet():

    matriceA = np.random.randint(0, 8, size=(3, 3))
    print("Matrice :", matriceA)
    print("Inverse : ",  np.linalg.inv(matriceA))
    print("Déterminant :", np.linalg.det(matriceA))


def systemeEquation():

    A = np.array([[4, 3, 2], [-2, 2, 3], [3, -5, 2]])
    B = np.array([25, -10, -4])

    for i in range(3):
        print(str(A[i][0]) + 'x+' + str(A[i][1]) + 'y+' + str(A[i][2])+'z='+str(B[i]))


    X = np.linalg.solve(A, B)

    print(X)


def valVectProp():
    A = np.array([[4, 3, 2], [-2, 2, 3], [3, -5, 2]])
    results = np.linalg.eig(A)
    print("Matrice : \n", A)
    print("Valeurs propores :\n", results[0])
    print("Vecteurs propores :\n", results[1])


if __name__ == '__main__':
    valVectProp()
