from PIL import Image
import matplotlib.pyplot as plt
import scipy
import numpy as np
from scipy.optimize import curve_fit


def image():
    image = Image.open("python.png")
    new_image = image.resize((400, 100))
    new_image = new_image.convert("RGB")
    new_image.save('imageResized.jpg')

    print(image.size)  # (601, 203)
    print(new_image.size)


def func(x, a, b, c):
    return a * np.exp(-b * x) + c


def approcherPoints():
    xdata = np.linspace(0, 4, 50)
    y = func(xdata, 2.5, 1.3, 0.5)
    np.random.seed(1729)
    y_noise = 0.2 * np.random.normal(size=xdata.size)
    ydata = y + y_noise
    plt.plot(xdata, ydata, 'b-', label='data')
    popt, pcov = scipy.optimize.curve_fit(func, xdata, ydata)
    plt.plot(xdata, func(xdata, *popt), 'r-', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))
    plt.show()


if __name__ == '__main__':
    image()