import csv
import sqlite3
import xml.etree.ElementTree as ET


class Region:
    def __init__(self,idRegion,nomRegion):
        self.idRegion = idRegion
        self.nomRegion = nomRegion


class Departement:
    def __init__(self,idDepartement,nomDepartement,idRegion):
        self.idDepartement = idDepartement
        self.nomDepartement = nomDepartement
        self.idRegion = idRegion


class Commune:
    def __init__(self, idCommune, nomCommune, totalPopulation, idDepartement):
        self.idCommune = idCommune
        self.nomCommune = nomCommune
        self.idDepartement = idDepartement
        self.totalPopulation = totalPopulation


def readCommuneCSV():
    crCommune = csv.reader(open("csv/communes.csv", "rt", encoding="iso-8859-1"))
    listCommunes = []

    for i in range(8):
        next(crCommune)

    for row in crCommune:
        idCommune = row[0].split(";")[5]
        nomCommune = row[0].split(";")[6]
        idDepartement = row[0].split(";")[2]
        totalPopulation = row[0].split(";")[9].replace(" ", "")

        listCommunes.append(Commune(idCommune, nomCommune, idDepartement, totalPopulation))

    return listCommunes


def readDepartementCSV():
    crDepartement = csv.reader(open("csv/departements.csv", "rt", encoding="iso-8859-1"))
    listDepartements = []

    for i in range(8):
        next(crDepartement)

    for row in crDepartement:
        idDepartement = row[0].split(";")[2]
        nomDepartement = row[0].split(";")[3]
        idRegion = row[0].split(";")[0]
        listDepartements.append(Departement(idDepartement, nomDepartement, idRegion))

    return listDepartements


def readRegionCSV():
    crRegion = csv.reader(open("csv/regions.csv", "rt", encoding="iso-8859-1"))
    listRegion = []

    for i in range(8):
        next(crRegion)

    for row in crRegion:
        idRegion = row[0].split(";")[0]
        nomRegion = row[0].split(";")[1]
        listRegion.append(Region(idRegion, nomRegion))

    return listRegion


def readNewRegionCSV():

    crZone = csv.reader(open("csv/zones-2016.csv", "rt", encoding="iso-8859-1"))
    listRegion = []

    for i in range(6):
        next(crZone)

    for row in crZone:
        if row[0].split(";")[0] == "REG":
            idRegion = row[0].split(";")[1]
            listRegion.append(Region(idRegion, ""))
    return listRegion


def createConnection(dbFile):

    sqliteConnection = None
    try:
        sqliteConnection = sqlite3.connect(dbFile)
    except sqlite3.Error as error:
        print(error)
    return sqliteConnection


def createTables(sqliteConnection):

    try:
        cursor = sqliteConnection.cursor()
        sqlCommune = """CREATE TABLE IF NOT EXISTS Commune
            (
                idCommune       INTEGER ,
                nomCommune      TEXT,
                totalPopulation FLOAT ,
                idDepartement   INTEGER
                        REFERENCES Departement (idDepartement)
            );"""

        sqlDepartment = """CREATE TABLE IF NOT EXISTS Departement
            (
                idDepartement  INTEGER,
                nomDepartement TEXT,
                idRegion       INTEGER
                    REFERENCES Region (idRegion)
            );"""

        sqlRegion = """CREATE TABLE IF NOT EXISTS Region
            (
                idRegion  INTEGER,
                nomRegion TEXT
            );
            """

        sqlNewRegion = """CREATE TABLE IF NOT EXISTS NouvelleRegion
            (
                idRegion  INTEGER
            );
            """

        cursor.execute(sqlRegion)
        cursor.execute(sqlNewRegion)
        cursor.execute(sqlDepartment)
        cursor.execute(sqlCommune)


    except sqlite3.Error as error:
        print(error)


def insertDataCommune(sqliteConnection, listCommunes):

    cursor = sqliteConnection.cursor()
    try:
        sqlDelete = """DELETE from Commune"""
        sql = """INSERT INTO Commune 
                    (idCommune, nomCommune, totalPopulation, idDepartement )
                    VALUES
                    (?,?,?,?);"""

        cursor.execute(sqlDelete)
        for commune in listCommunes:
            cursor.execute(sql, (commune.idCommune, commune.nomCommune, commune.idDepartement, commune.totalPopulation))
        sqliteConnection.commit()

    except sqlite3.Error as error:
        print("Failed to insert Python variable into sqlite table", error)


def insertDataDepartement(sqliteConnection, listDepartements):

    cursor = sqliteConnection.cursor()

    try:
        sqlDelete = """DELETE from Departement"""
        sql = """INSERT INTO Departement 
                    (idDepartement, nomDepartement, idRegion)
                    VALUES
                    (?,?,?);"""

        cursor.execute(sqlDelete)

        for departement in listDepartements:
            cursor.execute(sql, (departement.idDepartement, departement.nomDepartement, departement.idRegion))
        sqliteConnection.commit()

    except sqlite3.Error as error:
        print("Failed to insert Python variable into sqlite table", error)


def insertDataRegion(sqliteConnection, listRegion):

    cursor = sqliteConnection.cursor()

    try:
        sqlDelete = """DELETE from Region"""
        sql = """INSERT INTO Region 
                    (idRegion, nomRegion)
                    VALUES
                    (?,?);"""

        cursor.execute(sqlDelete)
        for region in listRegion:
            cursor.execute(sql, (region.idRegion, region.nomRegion))

        sqliteConnection.commit()

    except sqlite3.Error as error:
        print("Failed to insert Python variable into sqlite table", error)


def insertDataNouvelleRegion(sqliteConnection, listNouvelleRegion):

    cursor = sqliteConnection.cursor()

    try:
        sqlDelete = """DELETE from NouvelleRegion"""
        sql = """INSERT INTO NouvelleRegion 
                    (idRegion)
                    VALUES
                    (?);"""

        cursor.execute(sqlDelete)
        for region in listNouvelleRegion:
            cursor.execute(sql, (region.idRegion,))

        sqliteConnection.commit()

    except sqlite3.Error as error:
        print("Failed to insert Python variable into sqlite table", error)


def calculDepartmentPopulation(sqliteConnection):

    cursor = sqliteConnection.cursor()

    try:
        sql = """SELECT idDepartement, SUM(totalPopulation) FROM Commune GROUP BY idDepartement"""
        cursor.execute(sql)

        results = cursor.fetchall()
        for result in results:
            print(result)
            
    except sqlite3.Error as error:
        print("Failed to get data", error)


def calculRegionPopulation(sqliteConnection):

    cursor = sqliteConnection.cursor()

    try:
        sql = """ SELECT region.idRegion, SUM(commune.totalPopulation)
                FROM Commune commune, Departement departement, Region region
                WHERE departement.idDepartement = commune.idDepartement
                AND region.idRegion = departement.idRegion
                GROUP BY departement.idRegion"""

        cursor.execute(sql)
        results = cursor.fetchall()
        for result in results:
            print(result)

    except sqlite3.Error as error:
        print("Failed to get data", error)


def getSameCommune(sqliteConnection):

    cursor = sqliteConnection.cursor()

    try:
        sqlCommune = """SELECT nomCommune FROM Commune GROUP BY nomCommune Having COUNT(nomCommune) > 1"""

        cursor.execute(sqlCommune)
        results = cursor.fetchall()
        for result in results:
            sqlDepartement = """SELECT idDepartement from Commune where nomCommune = ?"""
            cursor.execute(sqlDepartement, result)
            listDept = list(cursor.fetchall())
            print(result[0],"", listDept)

    except sqlite3.Error as error:
        print("Failed to get data", error)


def getAllRegions(sqliteConnection):

    cursor = sqliteConnection.cursor()

    try:
        sql = """SELECT * FROM Region"""

        cursor.execute(sql)
        results = cursor.fetchall()
        listRegions = []
        for result in results:
            region = Region(result[0], result[1])
            listRegions.append(region)
        return listRegions

    except sqlite3.Error as error:
        print("Failed to get data", error)


def getAllDepartements(sqliteConnection):

    cursor = sqliteConnection.cursor()

    try:
        sql = """SELECT * FROM Departement"""

        cursor.execute(sql)
        results = cursor.fetchall()
        listDepartements = []
        for result in results:
            departement = Departement(result[0], result[1], result[2])
            listDepartements.append(departement)
        return listDepartements

    except sqlite3.Error as error:
        print("Failed to get data", error)


def getAllCommunes(sqlConnection):

    cursor = sqlConnection.cursor()

    try:
        sql = """SELECT * FROM Commune"""

        cursor.execute(sql)
        results = cursor.fetchall()
        listCommunes = []
        for result in results:
            commune = Commune(result[0], result[1], result[2], result[3])
            listCommunes.append(commune)
        return listCommunes

    except sqlite3.Error as error:
        print("Failed to get data", error)


def exportXML(sqlConnection):

    root = ET.Element('TP5')

    def prettify(element, indent='  '):
        queue = [(0, element)]  # (level, element)
        while queue:
            level, element = queue.pop(0)
            children = [(level + 1, child) for child in list(element)]
            if children:
                element.text = '\n' + indent * (level + 1)  # for child open
            if queue:
                element.tail = '\n' + indent * queue[0][0]  # for sibling open
            else:
                element.tail = '\n' + indent * (level - 1)  # for parent close
            queue[0:0] = children  # prepend so children come before siblings

    listRegions = getAllRegions(sqlConnection)
    for region in listRegions:
        regions = ET.SubElement(root, 'Région')
        idRegion = ET.SubElement(regions, 'IdRegion')
        nomRegion = ET.SubElement(regions, 'NomRegion')
        idRegion.text = str(region.idRegion)
        nomRegion.text = region.nomRegion

    listDepartements = getAllDepartements(sqlConnection)
    for departement in listDepartements:
        departements = ET.SubElement(root, 'Département')
        idDepartement = ET.SubElement(departements, 'IdDepartement')
        nomDepartement = ET.SubElement(departements, 'NomDepartement')
        idReg = ET.SubElement(departements, 'IdRegion')
        idDepartement.text = str(departement.idDepartement)
        nomDepartement.text = departement.nomDepartement
        idReg.text = str(departement.idRegion)

    listCommunes = getAllCommunes(sqlConnection)
    for commune in listCommunes:
        communes = ET.SubElement(root, 'Commune')
        idCommune = ET.SubElement(communes, 'IdCommune')
        nomCommune = ET.SubElement(communes, 'nomCommune')
        idDeprt = ET.SubElement(communes, 'IdDepartement')
        totalPopulation = ET.SubElement(communes, 'TotalPopulation')
        idCommune.text = str(commune.idCommune)
        nomCommune.text = commune.nomCommune
        idDeprt.text = str(commune.idDepartement)
        totalPopulation.text = str(commune.totalPopulation)

    prettify(root)
    tree = ET.ElementTree(root)
    tree.write('test.xml', encoding='UTF-8')


def importXML(sqliteConnection):

    tree = ET.parse('test.xml')
    root = tree.getroot()

    listRegions = []
    listDepartements = []
    listCommunes = []

    for regions in root.findall("Région"):
        region = Region(regions.find('IdRegion').text, regions.find('NomRegion').text)
        listRegions.append(region)

    for departements in root.findall("Département"):
        departement = Departement(departements.find('IdDepartement').text, departements.find('NomDepartement').text,
                                  departements.find('IdRegion').text)
        listDepartements.append(departement)

    for communes in root.findall("Commune"):
        commune = Commune(communes.find('IdCommune').text, communes.find('nomCommune').text,
                                  communes.find('IdDepartement').text, communes.find('TotalPopulation').text)
        listCommunes.append(commune)

    insertDataRegion(sqliteConnection, listRegions)
    insertDataDepartement(sqliteConnection, listDepartements)
    insertDataCommune(sqliteConnection, listCommunes)


def main():

    database = "dbINSEE.db"
    connection = createConnection(database)

    listCommunes = readCommuneCSV()
    listDepartements = readDepartementCSV()
    listRegions = readRegionCSV()


    with connection:
        #createTables(connection)
        #insertDataRegion(connection, listRegions)
        #insertDataDepartement(connection, listDepartements)
        #insertDataCommune(connection, listCommunes)
        #insertDataNouvelleRegion(connection, listNouvelleRegion)

        #calculDepartmentPopulation(connection)
        #calculRegionPopulation(connection)
        getSameCommune(connection)

        #exportXML(connection)
        #importXML(connection)

    connection.close()


if __name__ == '__main__':
    main()
