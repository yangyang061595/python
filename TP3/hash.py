from tkinter import *
from tkinter import messagebox as mb
import bcrypt
import random

def creatLogin(username, password):
    print("username entered :", username.get())
    print("password entered :", password.get())
    salt = bcrypt.gensalt()
    mdp = bcrypt.hashpw(password.get().encode(),salt)
    print("password entered :", mdp)
    with open("mdp.txt", "a") as fic:
        
        fic.write(username.get()+',')
        fic.write(mdp.decode())
        fic.write("\n")

def checkLogin(username):
    with open("mdp.txt", "r") as fic:
        line = fic.readline()
        user = line.split(",")[0]
        if user == username:
            mb.showwarning('True', 'login name exist in file')
        else:
            mb.showwarning('False', 'login name not exist in file')

if __name__ == "__main__":
    #window
    tkWindow = Tk()  
    tkWindow.geometry('400x150')  
    tkWindow.title('Tkinter Login Form - pythonexamples.org')

    #username label and text entry box
    usernameLabel = Label(tkWindow, text="User Name").grid(row=0, column=0)
    username = StringVar()
    usernameEntry = Entry(tkWindow, textvariable=username).grid(row=0, column=1)  

    #password label and password entry box
    passwordLabel = Label(tkWindow,text="Password").grid(row=1, column=0)  
    password = StringVar()
    
    passwordEntry = Entry(tkWindow, textvariable=password, show='*').grid(row=1, column=1)  

    

    #login button
    loginButton = Button(tkWindow, text="Login", command=lambda:creatLogin(username,password)).grid(row=4, column=0)  
    checkButton = Button(tkWindow, text="Login", command=lambda:checkLogin(username)).grid(row=4, column=2)  
    tkWindow.mainloop()