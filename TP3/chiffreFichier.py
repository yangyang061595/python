from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
import hashlib

if __name__ == "__main__":
    
    password = 'mot de passe'
    mdp = hashlib.md5(password.encode()).digest()
    print(mdp)
    cipher = AES.new(mdp, AES.MODE_EAX)

    

    filename =input("Entrez un nom de fichier\n")
    outputfilename = filename+".bin"
    with open(filename,"rb") as fic:
        plaintext = fic.read()
        ciphertext, tag = cipher.encrypt_and_digest(plaintext)
        
        with open(outputfilename,"wb") as ofic:
            [ ofic.write(x) for x in (cipher.nonce, tag, ciphertext) ]
            ofic.close()



#dechiffre le fichier
    file_in = open(outputfilename, "rb")
    nonce, tag, ciphertext = [ file_in.read(x) for x in (16, 16, -1) ]

    cipher = AES.new(mdp, AES.MODE_EAX, nonce)
    data = cipher.decrypt_and_verify(ciphertext, tag)

    print("file data:" +data.decode() )

