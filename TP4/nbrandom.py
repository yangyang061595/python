import random 
import matplotlib.patches as mpatches
import matplotlib.pyplot as plot
if __name__ == "__main__":
    liste = random.sample(range(500, 1000),10)
    liste2 = random.sample(range(500, 1000),10)
    xliste =  random.sample(range(10, 100),10)
    liste3 =  random.sample(range(500, 1000),10)

    print(xliste)
    print(liste)
    liste.sort()
    liste3.sort()
    xliste.sort()
    liste2.sort()
    plot.plot(liste,xliste,"r--");
    
    plot.arrow(550, 10, 10, 20, length_includes_head=True,
          head_width=10, head_length=10)
    plot.plot(liste2,xliste,"b-.");

    plot.plot(liste3,xliste,"g:");
    plot.ylabel("Label y")

    red_patch = mpatches.Patch(color = 'red',label ='The red data')
    plot.legend(handles = [red_patch])

    plot.xlabel("Label x")
    plot.show()
