import matplotlib.pyplot as plot
import matplotlib.pyplot as plot2
import matplotlib.pyplot as plot3
import matplotlib.pyplot as plot4
from matplotlib import cm
from numpy import random
from numpy import np

if __name__ == "__main__":
    ##Les données à mettre dans les graphiques
    listNumbersX = random.randint(100, size=(10))
    listNumbersY1 = random.randint(100, size=(10))
    listNumbersY2 = random.randint(100, size=(10))
    listNumbersY3 = random.randint(100, size=(10))

    ##Graphique
    plot.plot(listNumbersX, listNumbersY1, "r--", label="1ere courbe", linewidth=4)
    plot.plot(listNumbersX, listNumbersY2, "g^", label="2eme courbe", linewidth=20)
    plot.plot(listNumbersX, listNumbersY3, "bs", label="3eme courbe", linewidth=2)

    ##plot.annotate('Limite', arrowprops={'facecolor': 'black', 'shrink': 0.05})
    plot.ylabel('Axe des ordonnées')
    plot.xlabel('Axe des abscisses')
    plot.legend()
    plot.show()

    ##Histogramme
    n, bins, patches = plot2.hist(listNumbersY1, 50, facecolor='blue', alpha=0.5)
    plot2.ylabel('Axe des ordonnees')
    plot2.xlabel('Axe des abscisses')
    plot2.show()

    ##Camembert
    name = ['0-10', '10-20', '20-30', '30-40', '40-50', '50-60', '60-70', '70-80',
            '80-90', '90-100']
    data = listNumbersY1
    plot3.pie(data, labels=name, autopct='%1.1f%%', startangle=90, shadow=True)
    plot3.axis('equal')
    plot3.show()

    #Graphe 3D
    fig = plot4.figure()
    ax = fig.gca(projection='3d')
    X = listNumbersY1
    Y = listNumbersY2
    X, Y = np.meshgrid(X, Y)
    R = np.sqrt(X ** 2 + Y ** 2)
    Z = np.sin(R)
    surf = ax.plot_surface(listNumbersX, listNumbersY1, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    plot4.show()
