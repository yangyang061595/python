import matplotlib.pyplot as plt

if __name__ =="__main__":
    labels =( 'Allemagne','France','Belgique','Espagne')
    sizes = [15,80,45,40]
    print(labels)
    colors = ['yellowgreen','gold','lightskyblue','lightcoral']

    plt.pie(sizes,labels = labels, colors=colors,autopct='%1.1f%%',shadow=True,startangle=90)

    plt.axis('equal')
    plt.show()
