import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import np_utils
from keras.layers import Flatten,LSTM
from keras.callbacks import ModelCheckpoint
from keras import backend
from keras.layers import Dropout
import numpy as np
import os

os.environ['KMP_DUPLICATE_LIB_OK']='True'

n_timesteps = 240 

def load_data(fileName,sheetName):
    global n_timesteps
    df = pd.read_excel (fileName,usecols=[1,2,3,4],names=[ 'Open','High','Low','Close'],
    sheet_name=sheetName,header=None, )
    
    n_samples,n_features = df.shape[0] , df.shape[1]-1
    


    x = df[['Open','High','Low']]
    

    # extend x_train to 3D array because Conv1D accept only 3d input  
  
    # convert 2d to 1d array
    x =x.values.flatten()  
    
    #each new_samples include n_timesteps samples.  
    new_samples = n_samples//n_timesteps
    extra_samples = n_samples - new_samples*n_timesteps

    #remove extra samples and reshape x_train to [new_samples,n_timesteps,n_features] 
    x = x[:-(extra_samples*n_features)].reshape(new_samples,n_timesteps,n_features )


    #We consider if the last value in interval n_timesteps is bigger than the first one, the currency is going up. 
    label_value_list = np.array([ 1 if df.iloc[i, df.columns.get_loc('Close')] < df.iloc[i+n_timesteps-1, df.columns.get_loc('Close')]  else 0 
    for i in range(0,len(df["Close"])-n_timesteps+1, n_timesteps) ])

    y = label_value_list


    return x,y

# fit and evaluate a model
def evaluate_model(trainX, trainy, testX, testy):
    global n_timesteps

    verbose, epochs, batch_size = 0, 10, 32
    n_features ,n_outputs =  trainX.shape[2], 1
    print(n_features,n_outputs)#  4 1

    model = Sequential()
    model.add(LSTM(50, input_shape=(n_timesteps,n_features)))
    model.add(Dropout(0.5))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(n_outputs, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam',metrics=['accuracy'])


  
    # fit network
    
    filepath="LSTM-weights-improvement-{epoch:02d}-{val_accuracy:.2f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')
    callbacks_list = [checkpoint]

   
    model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size,validation_split=0.2, callbacks=callbacks_list,verbose=verbose)
    # evaluate model
    _ , accuracy = model.evaluate(testX, testy, batch_size=batch_size, verbose=0)
    return accuracy


if __name__ =="__main__":

    
    x_train,y_train = load_data(r'./ForexData/DAT_XLSX_EURUSD_M1_2018.xlsx',"2018")
    x_test,y_test = load_data(r'./ForexData/DAT_XLSX_EURUSD_M1_2019.xlsx',"2019")
    
    
    # concatenate data in two file
    x_train = np.concatenate((x_train, x_test[:-100]))

    y_train = np.concatenate((y_train, y_test[:-100]))

    # use 100 samples in the second file as test samples 
    x_test = x_test[-100:]
    y_test = y_test[-100:]


    print("accuracy:",evaluate_model(x_train,y_train,x_test,y_test)) 
   




   


