import pandas as pd
import numpy as np
import os


n_timesteps = 1 

def load_data(fileName,sheetName):
    global n_timesteps
    df = pd.read_excel (fileName,usecols=[1,2,3,4],names=[ 'Open','High','Low','Close'],
    sheet_name=sheetName,header=None, )
    
    n_samples,n_features = df.shape[0] , df.shape[1]-1
    


    x = df[['Open','High','Low']]
    


    #We consider if the next value is bigger than the previous one, the currency is going up. 
    label_value_list = np.array([ 1 if df.iloc[i, df.columns.get_loc('Close')] < df.iloc[i+1, df.columns.get_loc('Close')]  else 0 
    for i in range(0,len(df["Close"])-1) ])

    y = label_value_list


    return x,y

# fit and evaluate a model
def evaluate_model(testX, testy):
    
    predictY = np.array([ 1 if testy[i] < testy[i+1]  else 0 
    for i in range(0,len(testy)-1) ])

    count = 0
    for i in range(0,len(predictY)) :
        if predictY[i] == testy[i] :
            count += 1
    accuracy = count / len(predictY)

    return accuracy


if __name__ =="__main__":

    x_test,y_test = load_data(r'./ForexData/DAT_XLSX_EURUSD_M1_2019.xlsx',"2019")
    


    print("accuracy:",evaluate_model(x_test,y_test)) 
   

