import http.server
from http.server import HTTPServer, CGIHTTPRequestHandler

if __name__ =="__main__" :
    PORT = 8888
    server_address = ("",PORT)
    server = http.server.HTTPServer
    handler = CGIHTTPRequestHandler 
    handler.cgi_directories = ["/","/cgi_bin"]
    print("Serveur actif sur le port:",PORT)

    httpd = server(server_address,handler)
    httpd.serve_forever()
    


