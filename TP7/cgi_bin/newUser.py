#!/usr/bin/python3
import cgi

print("Content-type: text/html; charset=utf-8\n")
form = cgi.FieldStorage()

if form.getvalue("NewUserSuccess") == "Success":
    print("Create new user success")

html = """<!DOCTYPE html> <head>
    <title>Create new user</title>
</head>
<body>
<form action="/cgi_bin/checkNewUser.py" method="post">
<input type="text" name="name" value="nouvelle utilisateur " />
<input type="password" name="newMdp"/>
<input type="submit" name="send" value="Envoyer information au serveur">
</form>
</body>
</html>
"""

print(html)
