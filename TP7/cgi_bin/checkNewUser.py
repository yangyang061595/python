#!/usr/bin/python3
import sys
import cgi

form = cgi.FieldStorage() 
datafilename="cgi_bin/data.txt" 
#recuperer les infos envoyees par le web
def checkIfExist(username):
    with open(datafilename, "rt") as fic:
        lines = fic.readlines()
        for line in lines:
            trueUsername = line.split(";")[0].strip()
            if(username == trueUsername ):
                return True
        return False
name = form.getvalue('name')
mdp  = form.getvalue('newMdp')

if( name is None or  mdp is None):
    print ("Content-type:text/html")
    print ()
    print ("<html>")
    print("You have not input your new name and password")
    print("</html>")
else:
    if(not checkIfExist(name)):
        with open(datafilename,"at") as fic:
            fic.write(name+";"+mdp+"\n")
        print("Content-type:text/html")
        print("Refresh:1, URL=newUser.py?NewUserSuccess=Success")
    else:
        print ("Content-type:text/html")
        print ()
        print ("<html>")
        print("username already exite "+name)
        print ("</html>")
        
  

