import csv


class Etudiant:
    def __init__(self,prenom,nom,age):
        self.prenom = prenom
        self.nom = nom
        self.adresslec(prenom,nom)
        self.age = age

    def adresslec(self,prenom,nom):
        self.mail = prenom+ "."+nom+"@etu.univ-tours.fr"


def parser(file):
    cr = csv.reader(open(file, "rt"))
    listE = []
    for row in cr:
        prenom = row[0].split(";")[0]
        nom = row[0].split(";")[1]
        strp = row[0].split(";")[2]
        age = 2020 - int(strp.split("/")[2])
        listE.append(Etudiant(prenom, nom, age))
    for etu in listE:
        print(etu.prenom, etu.nom, etu.mail, etu.age)


if __name__ == "__main__":
    parser("fichetu.csv")
