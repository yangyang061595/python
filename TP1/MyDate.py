class MyDate :
    def __init__(self,year,month,day):
        self.year = year
        self.month = month
        self.day = day

    def __eq__(self,second):
        if not isinstance(second,MyDate ):
            return False
        else:
            return self.year == second.year and self.month ==second.month and self.day ==second.day

    def __lt__(self,second):
        if not isinstance(second,MyDate ):
            return False    
        elif self.year < second.year:
            return True
        else:
            if self.year == second.year:
                if self.month < second.month :
                    return True
                elif  self.month == second.month:
                    if self.day < second.day :
                        return True

        return False
        

if __name__ == "__main__":
    d1= MyDate(2020,9,10)
    d1copy= MyDate(2020,9,10)
    d2= MyDate(2020,9,11)
    d3= MyDate(2019,6,10)
    print(d1 == d1copy)
    print(d1==d2)
    print(d1<d2)
    print(d3<d2)