import datetime
import csv


class Date:

    def __init__(self, day, month, year):
        self.day = day
        self.month = month
        self.year = year

    def __eq__(self, otherDate):

        return self.day == otherDate.day and self.month == otherDate.month and self.year == otherDate.year

    def __lt__(self, otherDate):

        if self.year < otherDate.year:
            return 1
        elif self.year == otherDate.year and self.month < otherDate.month:
            return 1
        elif self.year == otherDate.year and self.month == otherDate.month and self.day < otherDate.day:
            return 1
        else:
            return 0


class Etudiant:

    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

    def adresselec(self):
        return self.name + "." + self.surname + "@etu.univ-tours.fr"

    def getAge(self):
        return self.age

    def __repr__(self):
        return self.name + ";" + self.surname + ";" + str(self.age)


def lecturefichier():
    with open('fichetu.csv') as csvFile:
        csvReader = csv.reader(csvFile, delimiter=";")
        listEtudiants = []
        for row in csvReader:
            name = row[0]
            surname = row[1]
            date = row[2]
            year = int(date.split('/')[-1])
            age = datetime.datetime.now().year - year

            etudiant = Etudiant(name, surname, age)

            listEtudiants.append(etudiant)

        print(listEtudiants)


date1 = Date(1, 1, 2000)
date2 = Date(2, 1, 2000)
person = Etudiant('nour', 'nasrallah', 12)
print(date1 < date2)
print(person.adresselec())
lecturefichier()
