from tkinter import * 

expression = ""


def press(num):
    global expression 
  
    # concatenation of string 
    expression = expression + str(num) 
    print(expression)

    # update the expression by using set method 
    equation.set(expression) 


def equalpress():
    try: 
  
        global expression 
        total = str(eval(expression)) 
        equation.set(total) 
        expression = ""
    except: 
        equation.set(" error ") 
        expression = "" 


def allclear(): 
    global expression 
    expression = "" 
    equation.set("") 


def clear():
    global expression 
    if not expression:
        expression.set(expression[:-1])


def donothing():
    pass


if __name__ == "__main__":
    fenetre = Tk()
   
    fenetre.title("Calculator")
    fenetre.geometry("480x270") 
    menubar = Menu(fenetre)
    aidemenu = Menu(menubar, tearoff=0)
    aidemenu.add_command(label="Aide", command=donothing)
    aidemenu.add_command(label="mode scientifique", command=donothing)
    menubar.add_cascade(label="mode", menu=aidemenu)

    equation = StringVar() 
    expression_input = Entry(fenetre,textvariable=equation)
    expression_input.grid(columnspan=4, ipadx=70) 


    equation.set('enter your expression') 

    allClear = Button(fenetre, text='AC', fg='black', bg='grey', 
                   command=allclear, height=1, width=7) 
    allClear.grid(row=2, column=0) 

    leftPare = Button(fenetre, text='(', fg='black', bg='grey', 
                   command=lambda: press("("), height=1, width=7) 
    leftPare.grid(row=2, column=1) 

    clear = Button(fenetre, text='C', fg='black', bg='grey', 
                   command=clear, height=1, width=7) 
    clear.grid(row=2, column=2) 

    divide = Button(fenetre, text=' / ', fg='black', background='yellow', 
                    command=lambda: press("/"), height=1, width=7) 
    divide.grid(row=2, column=3) 

    button7 = Button(fenetre, text=' 7 ', fg='black', bg='grey', 
                     command=lambda: press(7), height=1, width=7) 
    button7.grid(row=3, column=0) 
  
    button8 = Button(fenetre, text=' 8 ', fg='black', bg='grey', 
                     command=lambda: press(8), height=1, width=7) 
    button8.grid(row=3, column=1) 
  
    button9 = Button(fenetre, text=' 9 ', fg='black', bg='grey', 
                     command=lambda: press(9), height=1, width=7) 
    button9.grid(row=3, column=2) 

    multiply = Button(fenetre, text=' * ', fg='black', bg='gold', 
                      command=lambda: press("*"), height=1, width=7) 
    multiply.grid(row=3, column=3) 

    button4 = Button(fenetre, text=' 4 ', fg='black', bg='grey', 
                     command=lambda: press(4), height=1, width=7) 
    button4.grid(row=4, column=0) 
  
    button5 = Button(fenetre, text=' 5 ', fg='black', bg='grey', 
                     command=lambda: press(5), height=1, width=7) 
    button5.grid(row=4, column=1) 
  
    button6 = Button(fenetre, text=' 6 ', fg='black', bg='grey', 
                     command=lambda: press(6), height=1, width=7) 
    button6.grid(row=4, column=2) 

    minus = Button(fenetre, text=' - ', fg='black', bg='yellow', 
                   command=lambda: press("-"), height=1, width=7) 
    minus.grid(row=4, column=3) 

    button1 = Button(fenetre, text=' 1 ', fg='black', bg='grey', 
                     command=lambda: press(1), height=1, width=7) 
    button1.grid(row=5, column=0) 
  
    button2 = Button(fenetre, text=' 2 ', fg='black', bg='grey', 
                     command=lambda: press(2), height=1, width=7) 
    button2.grid(row=5, column=1) 

    button3 = Button(fenetre, text=' 3 ', fg='black', bg='grey', 
                     command=lambda: press(3), height=1, width=7) 
    button3.grid(row=5, column=2) 
    plus = Button(fenetre, text=' + ', fg='black', bg='yellow', 
                  command=lambda: press("+"), height=1, width=7) 
    plus.grid(row=5, column=3)
  
    button0 = Button(fenetre, text=' 0 ', fg='black', bg='grey', 
                     command=lambda: press(0), height=1, width=7) 
    button0.grid(row=6, column=0) 
  
    Decimal= Button(fenetre, text='.', fg='black', bg='grey', 
                    command=lambda: press('.'), height=1, width=7) 
    Decimal.grid(row=6, column=1) 
    
    rightPare = Button(fenetre, text=')', fg='black', bg='grey', 
                   command=lambda: press(")"), height=1, width=7) 
    rightPare.grid(row=6, column=2)
  
    equal = Button(fenetre, text=' = ', fg='black', bg='yellow', 
                   command=equalpress, height=1, width=7) 
    equal.grid(row=6, column=3)

    fenetre.config(menu=menubar)
    
    fenetre.mainloop() 