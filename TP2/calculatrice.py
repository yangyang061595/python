from tkinter import *

expression = ""

fenetre = Tk()


def press(number):
    global expression
    expression = expression + str(number)
    equation.set(expression)


def result():

    try:
        global expression
        total = str(eval(expression))
        equation.set(total)
        expression = ""
        print(total)

    except:
        equation.set(" error ")
        expression = ""


def clear():
    global expression
    expression = ""
    equation.set("")


equation = StringVar()
fenetre.title("Calculatrice")
fra1 = Frame(fenetre)
fra1.grid(row=1, column=0)
Entry(fra1, textvariable=equation).grid(row=1, columnspan=7, ipadx=100)
Button(fra1, text='9', width=8, command=lambda: press(9)).grid(row=2, column=2, padx=3, pady=3)
Button(fra1, text='8', width=8, command=lambda: press(8)).grid(row=2, column=1, padx=3, pady=3)
Button(fra1, text='7', width=8, command=lambda: press(7)).grid(row=2, column=0, padx=3, pady=3)
Button(fra1, text='6', width=8, command=lambda: press(6)).grid(row=3, column=2, padx=3, pady=3)
Button(fra1, text='5', width=8, command=lambda: press(5)).grid(row=3, column=1, padx=3, pady=3)
Button(fra1, text='4', width=8, command=lambda: press(4)).grid(row=3, column=0, padx=3, pady=3)
Button(fra1, text='3', width=8, command=lambda: press(3)).grid(row=4, column=2, padx=3, pady=3)
Button(fra1, text='2', width=8, command=lambda: press(2)).grid(row=4, column=1, padx=3, pady=3)
Button(fra1, text='1', width=8, command=lambda: press(1)).grid(row=4, column=0, padx=3, pady=3)
Button(fra1, text='0', width=8, command=lambda: press(0)).grid(row=5, column=1, padx=3, pady=3)
Button(fra1, text='+', width=8, command=lambda: press("+")).grid(row=2, column=5, padx=3, pady=3)
Button(fra1, text='-', width=8, command=lambda: press("-")).grid(row=3, column=5, padx=3, pady=3)
Button(fra1, text='*', width=8, command=lambda: press("*")).grid(row=2, column=6, padx=3, pady=3)
Button(fra1, text='/', width=8, command=lambda: press("/")).grid(row=3, column=6, padx=3, pady=3)
Button(fra1, text='.', width=8).grid(row=4, column=5, padx=3, pady=3)
Button(fra1, text='=', width=8, command=result).grid(row=4, column=6, padx=3, pady=3)
Button(fra1, text='C', width=8).grid(row=5, column=6, padx=3, pady=3)
Button(fra1, text='AC', width=8, command=clear).grid(row=5, column=5, padx=3, pady=3)


fenetre.mainloop()
